# Trabajo Integrador `- Unidad 1 (Aplicaciones Web)`

    Integrantes:
        -   Martín González Miranda
        -   Manuel Francisco Peña

    Api's Utilizadas:
        -   https://www.themoviedb.org/ -- The Movie Database, nos aporta el contenido de las películas.
        -   https://newsapi.org/  -- Para simular la zona de anuncios.
