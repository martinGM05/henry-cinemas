function funciones() {
  $(document).ready(function () {
      localStorage.removeItem("identificadorPelicula");
      $('.card').click(function () {
          let idPelicula = parseFloat($(this).find('.idpelis2').text())
          localStorage.setItem("identificadorPelicula", "" + idPelicula)
      });
  });
}

obtenerHistorial();
function obtenerHistorial(){
    $(document).ready(function () {
        if(!localStorage.getItem('historial')){
           alert('Upss, No hay nada que mostrar');
        }else{
            var datoshistorial=localStorage.getItem('historial')
            var historialUser=JSON.parse(datoshistorial)
            funciones()
            historialUser.forEach(respuesta=>{
                $('.contenedorHistorial').append('<div class="card" id="'+respuesta.identificador+'">'+'<a href="detallesPelicula.html" >'+
                '<div class="contenedorImagen"><img src="'+imagen(respuesta.urlImage)+'" alt="Avatar"></div>'+
                '<div class="container">'+
                  '<b class="idpelis2">'+respuesta.identificador+'</b>'+  
                  '<h4 id="nombrePelicula">'+respuesta.nombrePelicula+'</h4>'+'<h4 id="estreno">'+respuesta.fechaEstreno+'</h4>'+
                '</div>'+'</a>'+
              '</div>'+
          '</div>')

            })  
            }
    });
}


function imagen(articleImage) {
  if (articleImage != 'https://image.tmdb.org/t/p/w500null') {
      return articleImage;
  } else {
      return "../img/no-image.png";
  }
}