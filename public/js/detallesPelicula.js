const API_KEY = '5bada804875e6b795db769678504b6cc';
const BASE_URL = 'https://api.themoviedb.org/3/';
const IDIOMA = 'es-ES';
const id=localStorage.getItem("identificadorPelicula")
const API_INDIVIDUO= BASE_URL+'movie/'+id+'?api_key='+API_KEY+'&language='+IDIOMA;
const API_CAST = BASE_URL+'movie/'+id+'/credits?api_key='+API_KEY+'&language='+IDIOMA;
const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w500';
const IMAGE_BASE_URL_BACKDROP = 'https://image.tmdb.org/t/p/w1280';
const IMAGE_CAST_BASE_URL = 'https://image.tmdb.org/t/p/w185';
let contadorMenu = 0;

var historial
if(!localStorage.getItem('historial')){
    historial  = []
     
 }else{
     historial = []
     var datosHistorial=localStorage.getItem('historial')
     var historialUsuario=JSON.parse(datosHistorial)
     historialUsuario.forEach(re=>{
         historial.push(re)
     })
 }

$(document).ready(function(){
    prelicula();
});


$(".hamburger-btn").click(function(){
    if(contadorMenu % 2 != 0){
        $("#btnCarrito").show();
        $("#btnCompartir").show();
    }else{
        $("#btnCarrito").hide();
        $("#btnCompartir").hide();
    }
    contadorMenu++;
});



function prelicula(){
    $.ajax({
        url: API_INDIVIDUO,
        method: 'GET',
        success: function(resp){
            $(".pelicula").append(
                `<h1 id="tituloP">${resp.title}</h1>`+
                '<div class="imagenFondo">'+`<img src=${IMAGE_BASE_URL_BACKDROP+resp.backdrop_path}>`+
                '</div>'+
                `<div class="detalles"><div class="imagenP"><img id="imagenPelicula" src="${IMAGE_BASE_URL+resp.poster_path}"></div><p class="descripcionP">${resp.overview}</p></div>`+'<h1 id="repartoTitulo">Reparto</h1>'+'<div class="repartoActores"></div>'
            )
            $("#btnCarrito").append('<i class="fas fa-cart-plus" onclick="funcionCarrito('+id+')"></i>')
            $("#btnCompartir").append('<i class="fas fa-share-alt" onclick="redirigirdesdeDetPeliculas('+id+')"></i>')
         if(((historial.findIndex(element => element.identificador ===resp.id)))===-1){
                historial.push({nombrePelicula: resp.original_title, urlImage:IMAGE_BASE_URL+resp.poster_path, identificador: resp.id, fechaEstreno: resp.release_date})
                localStorage.setItem('historial', JSON.stringify(historial));
        }
        reparto();
        }
    })
}


function reparto(){
    $.ajax({
        url: API_CAST,
        method: 'GET',
        success: function(resp){
            for(let i=0;i<10;i++){
                let datosActor = resp.cast[i];
                let html = `
                <div class="detallesActores">
                    <h2 id="nombreActor">${datosActor.name}</h2>
                    <div id="detalleImagen">
                    <img src="${imagen(datosActor.profile_path)}">
                    </div>
                </div> 
                    `;
                $(".repartoActores").append(html);
            }
        }
    })
}


function imagen(profilePath){
    if(profilePath){
        return IMAGE_CAST_BASE_URL+profilePath;
    }else{
        return "../img/no-image.png";
    }
}
function redirigirdesdeDetPeliculas(idPeliculaCompartir) {
    $(document).ready(function () {
        localStorage.removeItem('idpelicompartir')
        localStorage.setItem("idpelicompartir", "" + idPeliculaCompartir)
        window.location.href = 'Compartir.html';
    });
}
