const API_KEY = '5bada804875e6b795db769678504b6cc';
const BASE_URL = 'https://api.themoviedb.org/3/';
const IDIOMA = 'es-ES';
const id = localStorage.getItem("idpelicompartir")
const API_INDIVIDUO = BASE_URL + 'movie/' + id + '?api_key=' + API_KEY + '&language=' + IDIOMA;
const API_CAST = BASE_URL + 'movie/' + id + '/credits?api_key=' + API_KEY + '&language=' + IDIOMA;
const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w500';
const IMAGE_BASE_URL_BACKDROP = 'https://image.tmdb.org/t/p/w1280';
const IMAGE_CAST_BASE_URL = 'https://image.tmdb.org/t/p/w185';




cargarDatosCompartir();
function cargarDatosCompartir() {
    $.ajax({
        url: API_INDIVIDUO,
        method: 'GET',
        success: function (resp) {
            $("#formulariocompartir").append(
                '<input class="controls" type="text" placeholder="Sujeto Correo" id="Subject" value="' + resp.title + '" disabled>' +
                '<br>' +
                '<textarea class="controls" name="comentarios" id="Message" disabled>' + resp.overview + '</textarea>' +
                '<br>' +
                '<input  class="botons" type="submit"  value="Compartir" id="EmailButton" onclick="SendMail()"><br>'+
                '<input  class="botons" type="submit"  value="Cancelar" id="CancelButton" onclick="CancelMail()">')
        }
    })
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(email).toLowerCase())) return alert(`El email "${email}" no es un correo válido`);
    return re.test(String(email).toLowerCase());
}

function SendMail() {
    $(document).ready(function () {
        let emailD = document.getElementById("emailD").value;
        let emailR = document.getElementById("emailR").value;
        if (emailD != "" && emailR != "") {
            if (validateEmail(emailD) && validateEmail(emailR)) {
                var body = document.getElementById("Message").value;
                var SubjectLine = "Te comparto esta pelicula: " + document.getElementById("Subject").value;
                var emaildestino = document.getElementById("emailD").value;
                window.location.href = "mailto:" + emaildestino + "?subject=" + SubjectLine + "&body=" + body;
            }
        }else{
            alert("Por favor, rellene todos los campos");
        }
    });
}



function CancelMail(){
    
    $(document).ready(function () {  
        window.location.href = 'peliculas.html';
    });
}

// Fin correo