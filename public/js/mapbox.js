mapboxgl.accessToken ="pk.eyJ1IjoibWFydGluLWdvbnphbGV6IiwiYSI6ImNrdHZuMTZwcTJiaDAycXAzZWxrN3JidHEifQ.WOUtAYc1fk7M_6_14N05QA";
let contadorBot = 0;
let contadorMenu = 0;

let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
    center: [-57.956139, -34.9227],
    zoom: 15,
});

const texto = new mapboxgl.Popup().setText("Catedral de la plata").addTo(map);

const marker1 = new mapboxgl.Marker({
    color: "#000",
}).setLngLat([-57.956139, -34.9227]).setPopup(texto).addTo(map);


$('#bot').click(function(){
    contadorBot++;
    if(contadorBot % 2 != 0){
        $('.contenedorBot').css('display', 'block');
        $('.contenedorBot').css('opacity', '1');
    }else{
        $('.contenedorBot').css('display', 'none');
        $('.contenedorBot').css('opacity', '0');
    }
});


$(".hamburger-btn").click(function(){
    if(contadorMenu % 2 != 0){
        $("#bot").show();
    }else{
        $("#bot").hide();
    }
    contadorMenu++;
});