obtenerCarrito();
var totalCarrito=0;
var totalelementosCarrito=0
function obtenerCarrito() {
    $(document).ready(function () {
        if (!localStorage.getItem('carro')) {
            alert('Upss, No hay nada que mostrar')
        } else {
            var datosCarro = localStorage.getItem('carro')
            var prueba = JSON.parse(datosCarro)
            prueba.forEach(respuesta => {
                $('.contenedorCarrito').append('<div class="card" id="' + respuesta.identificador + '">' +
                    '<h3>' + respuesta.nombrePelicula + '</h3>' + '<div class="contenedorImagen"><img src="' + respuesta.urlImage + '" alt="Avatar"></div>' +
                    '<div class="container">' +
                    '<b>' + respuesta.identificador + '</b>' + '<p id="precio">$'+respuesta.precioPelicula+'</p>' +
                    '<button type="button" onclick="borrarElemento(' + respuesta.identificador+',' + respuesta.precioPelicula+ ')">Eliminar</button>' +
                    '</div>' +
                    '</div>' +
                    '</div>')
                    totalCarrito+=respuesta.precioPelicula;
            })
            totalelementosCarrito=prueba.length
            $('#articulosT').append(''+prueba.length)
            $('#montoT').append(''+totalCarrito)
        }
    });
}

function borrarElemento(identificador, precioPelicula) {
    var auxtotalCarrito=totalCarrito-precioPelicula;
    totalCarrito=auxtotalCarrito;
    var auxtotalelementoscarrito=totalelementosCarrito-1;
    totalelementosCarrito=auxtotalelementoscarrito;
    let auxarray = JSON.parse(localStorage.getItem('carro'))
    let positionfind = auxarray.findIndex(element => element.identificador === identificador)
    auxarray.splice(positionfind, 1)
    let arregloActualizado = JSON.stringify(auxarray)
    localStorage.removeItem('carro')
    borrarhtml(identificador);
    localStorage.setItem('carro', arregloActualizado)
}

function borrarhtml(identificador) {
    $('#' + identificador).remove();
    document.getElementById("articulosT").innerHTML=""+totalelementosCarrito;
    document.getElementById("montoT").innerHTML=""+totalCarrito;
}