var ident;
function funcionCarrito(identificadorpeliculadetalles) {
    $(document).ready(function () {
        if (!localStorage.getItem('carro')) {
            var carrito = []
        } else {
            var carrito = []
            var datosCarro = localStorage.getItem('carro')
            var prueba = JSON.parse(datosCarro)
            prueba.forEach(re => {
                carrito.push(re)
            })
        }
        let idPelicula = parseFloat($(this).find('.idpelis').text())
        ident = idPelicula;
        var URL_INDIVIDUO = 'https://api.themoviedb.org/3/movie/' + identificadorpeliculadetalles + '?api_key=5bada804875e6b795db769678504b6cc&language=es';
        $.ajax({
            url: URL_INDIVIDUO,
            method: 'GET',
            success: function (resp) {
                var precioAleatorio = Math.round(Math.random() * (50 - 5000) + parseInt(5000));
                if(((carrito.findIndex(element => element.identificador ===resp.id)))===-1){
                    carrito.push({ nombrePelicula: resp.original_title, urlImage: IMAGE_BASE_URL + resp.poster_path, identificador: resp.id, precioPelicula: precioAleatorio })
                localStorage.setItem('carro', JSON.stringify(carrito));
                Toast.show(resp.original_title+" se añadió al carrito", "success");
              }else{
                  Toast.show(resp.original_title+" se encuentra ya en el carrito", "success");   
                  }
            }
        })
    });
}