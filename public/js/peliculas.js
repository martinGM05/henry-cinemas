const API_KEY = '5bada804875e6b795db769678504b6cc';
const BASE_URL = 'https://api.themoviedb.org/3/';
const IDIOMA = 'es-ES';
const API_PELICULAS_POPULARES = BASE_URL + 'movie/popular?api_key=' + API_KEY + '&language=' + IDIOMA + '&page=1';
const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w500';
const API_GENEROS = BASE_URL + 'genre/movie/list?api_key=' + API_KEY + '&language=' + IDIOMA + '&page=1';
var contadorPages = 1
var codigoGenero = 0;
var origen
var namePelicula, yearPelicula
let inputNombrePelicula = document.getElementById("NombrePeliculaB");
let inputAnioPelicula = document.getElementById("AnioPelicula");
let contadorBot = 0;
let contadorMenu = 0;


function funciones() {
    $(document).ready(function () {
        localStorage.removeItem("identificadorPelicula");
        $('.card').click(function () {
            let idPelicula = parseFloat($(this).find('.idpelis2').text())
            localStorage.setItem("identificadorPelicula", "" + idPelicula)
        });
    });
}
cargarDatosPopulares()
function cargarDatosPopulares() {
    $(".peliculas").empty();
    origen = 1;
    contadorPages = 1
    peticionajax(API_PELICULAS_POPULARES)
}


function redirigir(idPeliculaCompartir) {
    $(document).ready(function () {
        localStorage.removeItem('idpelicompartir')
        localStorage.setItem("idpelicompartir", "" + idPeliculaCompartir)
        window.location.href = 'Compartir.html';
    });
}


$('#bot').click(function(){
    contadorBot++;
    if(contadorBot % 2 != 0){
        $('.contenedorBot').css('display', 'block');
        $('.contenedorBot').css('opacity', '1');
    }else{
        $('.contenedorBot').css('display', 'none');
        $('.contenedorBot').css('opacity', '0');
    }
});


$(".hamburger-btn").click(function(){
    if(contadorMenu % 2 != 0){
        $("#bot").show();
    }else{
        $("#bot").hide();
    }
    contadorMenu++;
});


inputAnioPelicula.addEventListener('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        e.preventDefault();
    }
});


function vermas(origen) {
    $(document).ready(function () {
        contadorPages += 1
        if (origen === 1) {
            $(".cardMas").remove();
            const API_PELICULAS_POPULARES_PAGES = BASE_URL + 'movie/popular?api_key=' + API_KEY + '&language=' + IDIOMA + '&page=' + contadorPages;
            peticionajax(API_PELICULAS_POPULARES_PAGES)
        } if (origen === 2) {
            $(".cardMas").remove();
            var URL_FILTRO_NOMBREPAGES = 'https://api.themoviedb.org/3/search/movie?api_key=5bada804875e6b795db769678504b6cc&language=es-ES&page=' + contadorPages + '&query=' + namePelicula

            peticionajax(URL_FILTRO_NOMBREPAGES)
        } else if (origen === 3) {
            $(".cardMas").remove();
            var URL_FILTRO_NOMBREXANIOPAGES = 'https://api.themoviedb.org/3/search/movie?api_key=5bada804875e6b795db769678504b6cc&language=es-ES&page=' + contadorPages + '&query=' + namePelicula + '&year=' + yearPelicula
            peticionajax(URL_FILTRO_NOMBREXANIOPAGES)
        } else if (origen === 4) {
            $(".cardMas").remove();
            var URL_FILTRO_CATEGORIAPAGES = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=5bada804875e6b795db769678504b6cc&language=es-ES&page=' + contadorPages + '&with_genres=' + codigoGenero
            peticionajax(URL_FILTRO_CATEGORIAPAGES)
        }
        else if (origen === 5) {
            $(".cardMas").remove();
            var URL_FILTRO_SOLOCINEPAGE = 'https://api.themoviedb.org/3/movie/now_playing?api_key=5bada804875e6b795db769678504b6cc&language=es-ES&page=' + contadorPages
            peticionajax(URL_FILTRO_SOLOCINEPAGE)
        }
    })
}

llenarSelect();
select = document.getElementById('genero');

function llenarSelect() {
    fetch(API_GENEROS)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            let generos = data.genres;
            generos.forEach(function (genero) {
                let option = document.createElement('option');
                option.value = genero.id;
                option.innerHTML = genero.name;
                select.appendChild(option);
            });
        });
}

select.addEventListener('change', function () {
    codigoGenero = select.value;
    inputNombrePelicula.value = "";
    inputAnioPelicula.value = "";
});



function Buscar() {
    $(document).ready(function () {
       if (inputNombrePelicula.value != "" && inputAnioPelicula.value != "") {
            var nombrePelicula = document.getElementById("NombrePeliculaB").value;
            var anioPelicula = document.getElementById("AnioPelicula").value;
            namePelicula = nombrePelicula
            yearPelicula = anioPelicula
            contadorPages = 1
            var URL_FILTRO_NOMBREXANIO = 'https://api.themoviedb.org/3/search/movie?api_key=5bada804875e6b795db769678504b6cc&language=es-ES&query=' + nombrePelicula + '&year=' + anioPelicula
            origen = 3
            $(".peliculas").empty();
            peticionajax(URL_FILTRO_NOMBREXANIO)
        } else if (codigoGenero > 0 && inputAnioPelicula.value === "" && inputNombrePelicula.value === "") {
            var URL_FILTRO_CATEGORIA = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=5bada804875e6b795db769678504b6cc&language=es-ES&with_genres=' + codigoGenero
            origen = 4
            contadorPages = 1
            $(".peliculas").empty();
            peticionajax(URL_FILTRO_CATEGORIA)
        } else  if (inputNombrePelicula.value != "" && inputAnioPelicula.value === "") {
            var nombrePelicula = document.getElementById("NombrePeliculaB").value;
            namePelicula = nombrePelicula
            contadorPages = 1
            var URL_FILTRO_NOMBRE = 'https://api.themoviedb.org/3/search/movie?api_key=5bada804875e6b795db769678504b6cc&language=es-ES&query=' + nombrePelicula
            origen = 2
            $(".peliculas").empty();
            peticionajax(URL_FILTRO_NOMBRE)
        }else{
            Toast.show("Favor de elegir un filtro", "error");
        }
        inputNombrePelicula.value = "";
        inputAnioPelicula.value = "";
        select.selectedIndex = 0;
    });
}

function buscarPeliculasCine() {
    var URL_FILTRO_SOLOCINE = 'https://api.themoviedb.org/3/movie/now_playing?api_key=5bada804875e6b795db769678504b6cc&language=es-ES&page=1'
    $(".peliculas").empty();
    origen = 5
    contadorPages = 1
    peticionajax(URL_FILTRO_SOLOCINE)


}


function peticionajax(URL) {
    $.ajax({
        url: URL,
        method: 'GET',
        success: function (resp) {
            funciones();
            //console.log("tamanio "+ resp.results.length);
            var tamanioResp=resp.results.length;
            try {
               if(tamanioResp<10){
                for (let i = 0; i < tamanioResp; i++) {
                    let peli = resp.results[i];
                    $(".peliculas").append(
                        '<div class="card">' +
                        '<a href="detallesPelicula.html" >' + '<div class="contenedorImagen">' + '<img src="' + imagen(peli.poster_path) + '"   alt="Avatar" ><div hidden><p class="idpelis2">' + peli.id + '</p></div></div></a>' + '<h4 id="tPelicula"><b>' + peli.original_title + '</b></h4>' + '<h4 id="estreno"><b>' + peli.release_date + '</b></h4>' +
                        '<button  class="compartir" onclick="redirigir(' + peli.id + ')"><i class="fas fa-share-alt"></i>' +
                        '<button class="agregar-carrito" onclick="funcionCarrito(' + peli.id + ')"><i class="fas fa-shopping-basket"></i>' +
                        '<p class="idpelis">' + peli.id + '</p>' +
                        '</button>' + '</div>'
                    )
                }

               // let hml = '<div class="cardMas" onclick="vermas(' + origen + ')" ><h1>Ver más</h1></div>';
                //$(".peliculas").append(hml);
               }else{
                for (let i = 0; i < 10; i++) {
                    let peli = resp.results[i];
                    $(".peliculas").append(
                        '<div class="card">' +
                        '<a href="detallesPelicula.html" >' + '<div class="contenedorImagen">' + '<img src="' + imagen(peli.poster_path) + '"   alt="Avatar" ><div hidden><p class="idpelis2">' + peli.id + '</p></div></div></a>' + '<h4 id="tPelicula"><b>' + peli.original_title + '</b></h4>' + '<h4 id="estreno"><b>' + peli.release_date + '</b></h4>' +
                        '<button  class="compartir" onclick="redirigir(' + peli.id + ')"><i class="fas fa-share-alt"></i>' +
                        '<button class="agregar-carrito" onclick="funcionCarrito(' + peli.id + ')"><i class="fas fa-shopping-basket"></i>' +
                        '<p class="idpelis">' + peli.id + '</p>' +
                        '</button>' + '</div>'
                    )
                }

                let hml = '<div class="cardMas" onclick="vermas(' + origen + ')" ><h1>Ver más</h1></div>';
                $(".peliculas").append(hml);
               }
            } catch (e) {
               // alert("No hay peliculas")
            }
        }
    })

}

function imagen(articleImage) {
    if (articleImage) {
        return IMAGE_BASE_URL + articleImage;
    } else {
        return "../img/no-image.png";
    }
}
