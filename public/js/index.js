API_NEWS = 'https://api.newscatcherapi.com/v2/latest_headlines?countries=MX&topic=business';
const key_news = 'OJM0lLunB5uwDO5IR_HpdF6js7Av3NoBHSNB01bIW3E';
let contadorBot = 0;
let contadorMenu = 0;

$.ajax({
    url: API_NEWS,
    method: 'GET',
    headers: {
        'x-api-key': key_news
    },
    success: function(resp){
        for (let i = 0; i < 3; i++) {
            let article = resp.articles[i];
            let html = `
            <div>
                <img src="${imagen(article.media)}">
                <div>
                    <h5>${article.title}</h5>
                    <a id="enlace" href="${article.link}">Leer mas...</a>
                </div>
            </div>
            `;
            $('.sidebar').append(html);
        }
    }
})


$('#bot').click(function(){
    contadorBot++;
    if(contadorBot % 2 != 0){
        $('.contenedorBot').css('display', 'block');
        $('.contenedorBot').css('opacity', '1');
    }else{
        $('.contenedorBot').css('display', 'none');
        $('.contenedorBot').css('opacity', '0');
    }
});


$(".hamburger-btn").click(function(){
    if(contadorMenu % 2 != 0){
        $("#bot").show();
    }else{
        $("#bot").hide();
    }
    contadorMenu++;
});


function imagen(articleImage){
    if(articleImage){
        return articleImage;
    }else{
        return 'https://via.placeholder.com/300x200';
    }
}
